package webings

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// PREFIX
const (
	PREFIX_API = "/api/"
	PREFIX_WEB = "/web/"
)

// Methods
const (
	POST   = "POST"
	GET    = "GET"
	PUT    = "PUT"
	DELETE = "DELETE"
)

// STATUS MSG
const (
	NOT_FOUND    = "Page not found"
	INTERNAL_ERR = "An internal error occured. Please try again later."
	INV_TOK      = "Invalid token"
	UNAUTH       = "Unauthorized access"
	OK           = "OK"
	U_ALRDY_EX   = "User already exists"
)

func Respond(writer http.ResponseWriter, responseCode int, message interface{}) {
	writer.WriteHeader(responseCode)
	encoder := json.NewEncoder(writer)
	encoder.Encode(&message)
}

func Write200Cont(writer http.ResponseWriter, message interface{}) {
	Respond(writer, http.StatusOK, message)
}

func Write200(writer http.ResponseWriter) {
	Write200Cont(writer, OK)
}

func Write401(writer http.ResponseWriter) {
	Respond(writer, http.StatusUnauthorized, UNAUTH)
}

func Write404(writer http.ResponseWriter) {
	Respond(writer, http.StatusNotFound, NOT_FOUND)
}

func Write500(writer http.ResponseWriter) {
	Respond(writer, http.StatusInternalServerError, INTERNAL_ERR)
}

func IsGet(r *http.Request) bool {
	return checkMethod(r, GET)
}

func IsPost(r *http.Request) bool {
	return checkMethod(r, POST)
}

func IsPut(r *http.Request) bool {
	return checkMethod(r, PUT)
}

func IsDelete(r *http.Request) bool {
	return checkMethod(r, DELETE)
}

func checkMethod(r *http.Request, exp string) bool {
	return r.Method == exp
}

func InitHandlerPanic(err error, handler string) {
	panic(fmt.Sprintf("Could not register %s handler", handler))
}
