package webings

import (
	"fmt"
	"net/http"
)

type Endpoint interface {
	GetPath() string
	GetHandler() func(w http.ResponseWriter, r *http.Request)
}

type DefaultEndpoint struct {
	Path    string
	Handler func(w http.ResponseWriter, r *http.Request)
}

func (d DefaultEndpoint) GetPath() string {
	return d.Path
}

func (d DefaultEndpoint) GetHandler() func(w http.ResponseWriter, r *http.Request) {
	return d.Handler
}

func NewDefaultEndpoint(path string, handler func(w http.ResponseWriter, r *http.Request)) Endpoint {
	return &DefaultEndpoint{
		Path:    path,
		Handler: handler,
	}
}

type versionEndpoint struct {
	DefaultEndpoint
	Version string
}

func (v versionEndpoint) GetPath() string {
	return v.Version + "/" + v.DefaultEndpoint.GetPath()
}

type ApiEndpoint struct {
	versionEndpoint
}

func (a ApiEndpoint) GetPath() string {
	return PREFIX_API + a.versionEndpoint.GetPath()
}

func NewApiEndpoint(path string, version string, handler func(w http.ResponseWriter, r *http.Request)) Endpoint {
	endPoint := &ApiEndpoint{}
	endPoint.Version = version
	endPoint.Path = path
	endPoint.Handler = handler
	return endPoint
}

type WebEndpoint struct {
	DefaultEndpoint
}

func (w WebEndpoint) GetPath() string {
	return PREFIX_WEB + w.DefaultEndpoint.GetPath()
}

func NewWebEndpoint(path string, handler func(w http.ResponseWriter, r *http.Request)) Endpoint {
	endPoint := &WebEndpoint{}
	endPoint.Path = path
	endPoint.Handler = handler
	return endPoint
}

type EndpointList map[string]Endpoint

var (
	endpoints EndpointList
)

func RegisterEndpoint(endpoint Endpoint, name string) error {
	if endpoints == nil {
		endpoints = make(EndpointList)
	}

	_, ok := endpoints[name]
	if ok {
		return fmt.Errorf("Endpoint already exists")
	}

	endpoints[name] = endpoint
	return nil
}

func GetEndpoints() map[string]Endpoint {
	return (map[string]Endpoint(endpoints))
}
